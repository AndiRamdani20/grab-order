package com.graborder.be.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graborder.be.dto.request.LoginRequest;
import com.graborder.be.dto.request.RegisterRequest;
import com.graborder.be.service.UserService;

@RestController
@RequestMapping("user-management")
public class UserController {
    
    @Autowired
    private UserService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<Object> register(@RequestBody RegisterRequest request){
        return userService.registerUser(request);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<Object> login(@RequestBody LoginRequest request){
        return userService.loginUser(request);
    }
}
