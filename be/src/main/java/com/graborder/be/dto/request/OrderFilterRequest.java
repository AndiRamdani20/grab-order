package com.graborder.be.dto.request;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderFilterRequest {
    private String customerName;
    private String foodName;
    private String status;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate orderDate;
}
