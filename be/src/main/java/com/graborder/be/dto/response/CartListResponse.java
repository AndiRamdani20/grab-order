package com.graborder.be.dto.response;

import java.util.List;

import com.graborder.be.dto.response.OrderHistoryResponse.CustomerInfo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CartListResponse {
    private Long cartId;
    private CustomerInfo customer;
    private List<FoodInfoDTO> foods;
    private Integer totalPrice;
    private int totalFood;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class FoodInfoDTO {
        private Long foodId;
        private String foodName;
        private int price;
        private int qty;
    }
}
