package com.graborder.be.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodListResponse {
    private List<Foods> foods;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Foods {
        private Long foodId;
        private String foodName;
        private int price;
    }
}
