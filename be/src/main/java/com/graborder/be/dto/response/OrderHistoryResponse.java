package com.graborder.be.dto.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderHistoryResponse {
    
    private Long orderId;
    private CustomerInfo customer;
    private String status;
    private int totalHarga;
    private int qty;
    private String orderDate;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CustomerInfo{
        private Long customerId;
        private String fullname;
    }
}
