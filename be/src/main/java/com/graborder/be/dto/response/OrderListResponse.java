package com.graborder.be.dto.response;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderListResponse {
    private Long orderId;
    private String status;
    private int totalPrice;
    private int qty;
    private Timestamp orderDate;
}
