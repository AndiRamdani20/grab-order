package com.graborder.be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderStatisticResponse {
    
    private Long totalOrders;
    private OrderByStatus byStatus;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderByStatus{
        private Long ongoing;
        private Long completed;
        private Long cancelled;
    }
}
