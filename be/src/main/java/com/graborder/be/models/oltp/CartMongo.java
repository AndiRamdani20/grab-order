package com.graborder.be.models.oltp;

import java.util.List;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "cart")
public class CartMongo {
    
    @Transient
    public static final String SEQUENCE_NAME = "cart_sequence";

    @Id
    private Long id;

    private UserMongo user;
    private FoodMongo food;
    private List<FoodCartInfo> foods;
    private Integer totalPrice;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class FoodCartInfo {
        private Long id;
        private String foodName;
        private int price;
        private int qty;
    }
}
