package com.graborder.be.models.oltp;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "foods")
public class FoodMongo {
    
    @Transient
    public static final String SEQUENCE_NAME = "foods_sequence";

    @Id
    private Long id;

    @Field(name = "food_name")
    private String foodName;

    @Field(name = "price")
    private Integer price;
}
