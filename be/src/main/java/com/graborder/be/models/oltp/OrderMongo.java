package com.graborder.be.models.oltp;

import java.util.List;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.graborder.be.models.oltp.CartMongo.FoodCartInfo;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "orders")
public class OrderMongo {
    
    @Transient
    public static final String SEQUENCE_NAME = "orders_sequence";

    @Id
    private Long id;

    private Customer customer;

    @Field("order_date")
    private String orderDate;
    
    @Field("total_price")
    private int total;

    private Integer qty;
    private String status;
    private OrderDetail detail;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Customer {
        private Long id;
        private String username;
        private String fullname;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OrderDetail {
        private List<FoodCartInfo> foodCart;
    }
}
