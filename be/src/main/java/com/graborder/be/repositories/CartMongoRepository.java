package com.graborder.be.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.graborder.be.models.oltp.CartMongo;
import com.graborder.be.models.oltp.FoodMongo;
import com.graborder.be.models.oltp.UserMongo;

public interface CartMongoRepository extends MongoRepository<CartMongo, Long> {
    Optional<CartMongo> findByUserAndFood(UserMongo user, FoodMongo food);

    Optional<CartMongo> findAllByUser_Id(Long userId);

    Optional<CartMongo> findByUser(UserMongo user);
}
