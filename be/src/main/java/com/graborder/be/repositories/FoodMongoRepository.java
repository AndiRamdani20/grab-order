package com.graborder.be.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.graborder.be.models.oltp.FoodMongo;

public interface FoodMongoRepository extends MongoRepository<FoodMongo, Long> {
    
}
