package com.graborder.be.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.graborder.be.models.olap.Food;

public interface FoodRepository extends JpaRepository<Food, Long>{
    
}
