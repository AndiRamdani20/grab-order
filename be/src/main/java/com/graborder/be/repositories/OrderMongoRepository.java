package com.graborder.be.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.graborder.be.models.oltp.OrderMongo;

public interface OrderMongoRepository extends MongoRepository<OrderMongo, Long> {
    List<OrderMongo> findAllByCustomer_Id(Long userId);

    @Query("{ $and: [{ 'status': 'Ongoing' }, { 'customer.id': ?0 }] }")
    List<OrderMongo> findOngoingOrderByCustomerId(Long customerId);
}
