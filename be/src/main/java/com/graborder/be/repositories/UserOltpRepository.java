package com.graborder.be.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.graborder.be.models.oltp.UserMongo;

public interface UserOltpRepository extends MongoRepository<UserMongo, Long> {
    UserMongo findByUsername(String username);

    Boolean existsByUsername(String username);
}
