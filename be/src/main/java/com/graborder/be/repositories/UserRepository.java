package com.graborder.be.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.graborder.be.models.olap.User;

public interface UserRepository extends JpaRepository<User, Long>{
    
}