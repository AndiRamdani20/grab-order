package com.graborder.be.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.graborder.be.dto.request.CartRequest;
import com.graborder.be.dto.response.CartListResponse;
import com.graborder.be.dto.response.CartListResponse.FoodInfoDTO;
import com.graborder.be.dto.response.MessageResponse;
import com.graborder.be.dto.response.OrderHistoryResponse.CustomerInfo;
import com.graborder.be.dto.response.ResponseBodyDTO;
import com.graborder.be.exception.classes.DataNotFoundException;
import com.graborder.be.models.oltp.CartMongo;
import com.graborder.be.models.oltp.CartMongo.FoodCartInfo;
import com.graborder.be.models.oltp.FoodMongo;
import com.graborder.be.models.oltp.UserMongo;
import com.graborder.be.repositories.CartMongoRepository;
import com.graborder.be.repositories.FoodMongoRepository;
import com.graborder.be.repositories.UserOltpRepository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CartService {
      @Autowired
      private CartMongoRepository cartMongoRepository;

      @Autowired
      private UserOltpRepository userOltpRepository;

      @Autowired
      private FoodMongoRepository foodMongoRepository;

      @Autowired
      private GenerateSequenceService generateSequenceService;

      private static final HttpStatus statusOk = HttpStatus.OK;

      @Transactional
      public ResponseEntity<Object> addCart(CartRequest request){
      UserMongo user = userOltpRepository.findById(request.getUserId())
            .orElseThrow(() -> new DataNotFoundException("Data user tidak dapat ditemukan !"));
      FoodMongo food = foodMongoRepository.findById(request.getFoodId())
            .orElseThrow(() -> new DataNotFoundException("Data makanan tidak dapat ditemukan !"));

      // Cari keranjang untuk pengguna tertentu
      Optional<CartMongo> cartExist = cartMongoRepository.findByUser(user);

      // Buat objek FoodCartInfo baru
      FoodCartInfo newFood = FoodCartInfo.builder()
            .id(food.getId())
            .foodName(food.getFoodName())
            .price(food.getPrice()) 
            .qty(1)
            .build();

      if(cartExist.isPresent()){
            // Jika keranjang untuk pengguna sudah ada
            CartMongo existingCart = cartExist.get();

            // Periksa apakah makanan sudah ada di dalam keranjang
            boolean isFoodExist = existingCart.getFoods().stream()
                  .anyMatch(data -> data.getId().equals(food.getId()));

            if(isFoodExist){
                  // Jika makanan sudah ada, tambahkan jumlah dan harga
                  FoodCartInfo existingFood = existingCart.getFoods().stream()
                  .filter(data -> data.getId().equals(food.getId())).findFirst().get();
                  existingFood.setQty(existingFood.getQty() + 1);
                  existingFood.setPrice(food.getPrice() * existingFood.getQty());
            } else {
                  // Jika makanan belum ada, tambahkan ke dalam keranjang
                  existingCart.getFoods().add(newFood);
            }

            // Hitung ulang total harga
            existingCart.setTotalPrice(existingCart.getTotalPrice() + food.getPrice());

            // Simpan keranjang yang sudah ada
            cartMongoRepository.save(existingCart);
      } else {
            // Jika keranjang belum ada, buat keranjang baru
            CartMongo newCart = CartMongo.builder()
                  .id(generateSequenceService.generateSequence(CartMongo.SEQUENCE_NAME))
                  .user(user)
                  .foods(Collections.singletonList(newFood))
                  .totalPrice(food.getPrice())
                  .build();

            // Simpan keranjang baru
            cartMongoRepository.save(newCart);
      }

      // Buat pesan respons
      String message = "Makanan " + food.getFoodName() + " berhasil ditambahkan ke keranjang";

      MessageResponse messageResponse =  MessageResponse.builder()
            .message(message)
            .statusCode(statusOk.value())
            .status(statusOk.getReasonPhrase())
            .build();

      // Log pesan
      log.info(message);

      // Kembalikan respons
      return new ResponseEntity<>(messageResponse, statusOk);
      }

      public ResponseEntity<Object> deleteFromCart(CartRequest request) {
            UserMongo user = userOltpRepository.findById(request.getUserId())
                  .orElseThrow(() -> new DataNotFoundException("Data user tidak dapat ditemukan !"));
      
            // Ambil keranjang berdasarkan pengguna (user)
            CartMongo cartExist = cartMongoRepository.findByUser(user)
                  .orElseThrow(() -> new DataNotFoundException("Data cart tidak ditemukan"));
      
            // Ambil makanan yang akan dihapus dari keranjang
            FoodMongo food = foodMongoRepository.findById(request.getFoodId())
                  .orElseThrow(() -> new DataNotFoundException("Data makanan tidak dapat ditemukan !"));
      
            // Cari makanan yang akan dihapus dari keranjang
            Optional<FoodCartInfo> foodToRemove = cartExist.getFoods().stream()
                  .filter(foodInfo -> foodInfo.getId().equals(request.getFoodId()))
                  .findFirst();
      
            // Jika makanan ditemukan dalam keranjang, hapus satu quantity
            if (foodToRemove.isPresent()) {
                  FoodCartInfo foodInfo = foodToRemove.get();
                  int currentQty = foodInfo.getQty();
      
                // Jika quantity lebih dari satu, kurangi satu quantity dan kurangi harga
            if (currentQty > 1) {
                  foodInfo.setQty(currentQty - 1);
                  foodInfo.setPrice(foodInfo.getPrice() - foodInfo.getPrice() / currentQty); // Kurangi harga sesuai dengan harga per quantity
            } else {
                    // Jika quantity hanya satu, hapus makanan dari keranjang
                  cartExist.getFoods().remove(foodInfo);
            }
      
                // Update total harga keranjang
            cartExist.setTotalPrice(
            cartExist.getFoods().stream()
                        .mapToInt(info -> info.getPrice())
                        .sum());
      
                // Simpan perubahan pada keranjang
            cartMongoRepository.save(cartExist);
      
            String message = "Quantity makanan " + food.getFoodName() + " dihapus dari keranjang";
            MessageResponse messageResponse = MessageResponse.builder()
                        .message(message)
                        .statusCode(statusOk.value())
                        .status(statusOk.getReasonPhrase())
                        .build();
      
            log.info(message);
      
            return new ResponseEntity<>(messageResponse, statusOk);
            } else {
                  throw new DataNotFoundException("Data makanan tidak ditemukan dalam keranjang !");
            }
      }                       

      public ResponseEntity<Object> getDisplayListCart(Long userId){
            CartMongo cartData = cartMongoRepository.findAllByUser_Id(userId)
                  .orElseThrow(() -> new DataNotFoundException("Data Cart tidak ditemukan"));

            int totalFoods = cartData.getFoods().size();
            int totalFoodOrder = cartData.getFoods().stream().mapToInt(FoodCartInfo::getQty).sum();

            List<FoodInfoDTO> foodList = cartData.getFoods().stream()
                  .map(data -> new FoodInfoDTO(data.getId(), data.getFoodName(), data.getPrice(), data.getQty())).toList();

            CartListResponse dataResponse = CartListResponse.builder()
                  .cartId(cartData.getId())
                  .customer(new CustomerInfo(
                        cartData.getUser().getId(), 
                        cartData.getUser().getFullname()))
                  .foods(foodList)
                  .totalPrice(cartData.getTotalPrice())
                  .totalFood(totalFoodOrder)
                  .build();

            ResponseBodyDTO responseBodyDTO = ResponseBodyDTO.builder()
                  .total(totalFoods)
                  .data(dataResponse)
                  .message("Berhasil memuat data keranjang")
                  .code(statusOk.value())
                  .status(statusOk.getReasonPhrase())
                  .build();

            return new ResponseEntity<>(responseBodyDTO, statusOk);
      }
}