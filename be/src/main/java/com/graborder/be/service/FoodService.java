package com.graborder.be.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.graborder.be.dto.response.ResponseBodyDTO;
import com.graborder.be.exception.classes.DataNotFoundException;
import com.graborder.be.models.oltp.FoodMongo;
import com.graborder.be.repositories.FoodMongoRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FoodService {
    @Autowired
    FoodMongoRepository foodMongoRepository;

    private static final HttpStatus statusOk = HttpStatus.OK;

    public ResponseEntity<Object> getAllFood(){
        List<FoodMongo> food = foodMongoRepository.findAll();
        if(food.isEmpty()){
            throw new DataNotFoundException("Data food tidak ditemukan");
        }

        String messageSuccess = "Berhasil memuat data food";

        ResponseBodyDTO response = ResponseBodyDTO.builder()
            .total(food.size())
            .data(food)
            .message(messageSuccess)
            .code(statusOk.value())
            .status(statusOk.getReasonPhrase())
            .build();
        
        log.info(messageSuccess);

        return new ResponseEntity<Object>(response, statusOk);
    }
}
