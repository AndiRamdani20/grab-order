package com.graborder.be.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.graborder.be.dto.request.OrderFilterRequest;
import com.graborder.be.dto.request.OrderRequest;
import com.graborder.be.dto.response.MessageResponse;
import com.graborder.be.dto.response.OrderHistoryResponse;
import com.graborder.be.dto.response.OrderHistoryResponse.CustomerInfo;
import com.graborder.be.dto.response.OrderListResponse;
import com.graborder.be.dto.response.OrderStatisticResponse;
import com.graborder.be.dto.response.OrderStatisticResponse.OrderByStatus;
import com.graborder.be.dto.response.ResponseBodyDTO;
import com.graborder.be.exception.classes.BadRequestException;
import com.graborder.be.exception.classes.DataNotFoundException;
import com.graborder.be.models.olap.Order;
import com.graborder.be.models.oltp.CartMongo;
import com.graborder.be.models.oltp.OrderMongo;
import com.graborder.be.models.oltp.OrderMongo.Customer;
import com.graborder.be.models.oltp.OrderMongo.OrderDetail;
import com.graborder.be.models.oltp.UserMongo;
import com.graborder.be.repositories.CartMongoRepository;
import com.graborder.be.repositories.OrderMongoRepository;
import com.graborder.be.repositories.OrderRepository;
import com.graborder.be.repositories.UserOltpRepository;
import com.graborder.be.service.kafkaservice.KafkaProducerService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderService {
    
    @Autowired
    private OrderMongoRepository orderMongoRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserOltpRepository userOltpRepository;

    @Autowired
    private CartMongoRepository cartMongoRepository;

    @Autowired
    private GenerateSequenceService generateSequenceService;

    @Autowired
    private KafkaProducerService kafkaProducerService;

    private static final HttpStatus statusOk = HttpStatus.OK;
    private static final String ORDER_NOT_FOUND = "Data Order tidak ditemukan";
    private static final String ORDER_SUCCESS = "Berhasil memuat data Order";
    private static final String ORDER_STATISTIC = "Berhasil memuat data Statistic Order";
    private static final String ONGOING = "Ongoing";
    private static final String COMPLETED = "Completed";
    private static final String CANCELLED = "Cancelled";

    @Transactional
    public ResponseEntity<Object> createOrder(Long userId){
        UserMongo user = userOltpRepository.findById(userId).orElseThrow(() -> new DataNotFoundException("User tidak ditemukan"));
        CartMongo cart = cartMongoRepository.findAllByUser_Id(userId).orElseThrow(() -> new DataNotFoundException("Cart tidak ditemukan"));

        int quantity = cart.getFoods().stream().mapToInt(food -> food.getQty()).sum();
        OrderMongo newOrder = OrderMongo.builder()
            .id(generateSequenceService.generateSequence(OrderMongo.SEQUENCE_NAME))
            .customer(new Customer(user.getId(), user.getUsername(), user.getFullname()))
            .orderDate(Timestamp.valueOf(LocalDateTime.now()).toString())
            .total(cart.getTotalPrice())
            .qty(quantity)
            .status(ONGOING)
            .detail(OrderDetail.builder()
                .foodCart(cart.getFoods()).build())
            .build();

        orderMongoRepository.save(newOrder);
        cartMongoRepository.save(cart);
        kafkaProducerService.sendOrderData(newOrder);

        String message = "Berhasil membuat order!" + user.getFullname();

        ResponseBodyDTO response = ResponseBodyDTO.builder()
                        .total(cart.getFoods().size())
                        .data(newOrder)
                        .message(message)
                        .code(statusOk.value())
                        .status(statusOk.getReasonPhrase())
                        .build();
        
        log.info(message);

        return new ResponseEntity<>(response, statusOk);
    }

    public ResponseEntity<Object> updateOrder(OrderRequest orderRequest){
        OrderMongo orderMongo = orderMongoRepository.findById(orderRequest.getOrderId())
            .orElseThrow(() -> new DataNotFoundException(ORDER_NOT_FOUND));
        log.info(orderMongo.getStatus());
        if(orderMongo.getStatus().equals(ONGOING)){
            orderMongo.setStatus(orderRequest.getAction());
            orderMongoRepository.save(orderMongo);
            kafkaProducerService.sendOrderData(orderMongo);

            log.info("Data Orders = " + orderMongo);

            String message = "Berhasil mengubah status Order (" + orderRequest.getAction()
                                + ") untuk Order user: "
                                + orderMongo.getCustomer().getFullname();

                MessageResponse response = MessageResponse.builder()
                                .message(message)
                                .statusCode(statusOk.value())
                                .status(statusOk.getReasonPhrase())
                                .build();

                log.info(message);
                return new ResponseEntity<>(response, statusOk);
        } else {
                throw new BadRequestException("Anda tidak bisa mengubah Order yang sudah selesai");
        }
    }

    public ResponseEntity<Object> getUserOrder(Long userId){
        List<OrderMongo> orderList = orderMongoRepository.findAllByCustomer_Id(userId);
        if (orderList.isEmpty()){
            throw new DataNotFoundException(ORDER_NOT_FOUND);
        } else {
            List<OrderListResponse> datas = orderList.stream().map(data -> new OrderListResponse(
                data.getId(),
                data.getStatus(), data.getTotal(), data.getDetail().getFoodCart().size(),
                Timestamp.valueOf(data.getOrderDate()))).toList();
            
            ResponseBodyDTO response = ResponseBodyDTO.builder()
                .total(orderList.size())
                .data(datas)
                .message(ORDER_SUCCESS)
                .code(statusOk.value())
                .status(statusOk.getReasonPhrase())
                .build();

            log.info(ORDER_SUCCESS);

            return new ResponseEntity<>(response, statusOk);
        }
    }

    public ResponseEntity<Object> getOrderById(Long orderId){
        OrderMongo orderData = orderMongoRepository.findById(orderId).orElseThrow(() -> new DataNotFoundException(ORDER_NOT_FOUND));

        ResponseBodyDTO response = ResponseBodyDTO.builder()
                        .total(1)
                        .data(orderData)
                        .message(ORDER_SUCCESS)
                        .code(statusOk.value())
                        .status(statusOk.getReasonPhrase())
                        .build();

        log.info(ORDER_SUCCESS);

        return new ResponseEntity<>(response, statusOk);
    }

    public ResponseEntity<Object> getOngoingOrder(Long customerId){
        List<OrderMongo> orderList = orderMongoRepository.findOngoingOrderByCustomerId(customerId);

        if(orderList.isEmpty()){
            throw new DataNotFoundException(ORDER_NOT_FOUND);
        } else {
            ResponseBodyDTO response = ResponseBodyDTO.builder()
                .total(orderList.size())
                .data(orderList)
                .message(ORDER_SUCCESS)
                .code(statusOk.value())
                .status(statusOk.getReasonPhrase())
                .build();

            log.info(ORDER_SUCCESS);

            return new ResponseEntity<>(response, statusOk);
        }
    }

    public ResponseEntity<Object> getHistoricalOrder(OrderFilterRequest request, Pageable page){
        Specification<Order> orderSpecification = OrderSpecification.filterOrder(request);
        Page<Order> orderData = orderRepository.findAll(orderSpecification, page);

        if (orderData.isEmpty()) {
            throw new DataNotFoundException(ORDER_NOT_FOUND);
        } else {
            List<OrderHistoryResponse> datas = orderData.stream()
                .map(data -> new OrderHistoryResponse(
                    data.getOrderId(),
                    new CustomerInfo(data.getUser().getUserId(),
                                    data.getUser().getFullname()),
                    data.getStatus(), data.getTotalPrice(), data.getQty(), data.getOrderDate().toString()
                )).toList();
            
            ResponseBodyDTO response = ResponseBodyDTO.builder()
                    .total((int) orderRepository.count(orderSpecification))
                    .data(datas)
                    .message(ORDER_SUCCESS)
                    .code(statusOk.value())
                    .status(statusOk.getReasonPhrase())
                    .build();

            log.info(ORDER_SUCCESS);

            return new ResponseEntity<>(response, statusOk);
        }
    }

    public ResponseEntity<Object> getOrderStatistic() {
                long totalOrders = orderRepository.count();
                long totalOngoingOrders = orderRepository.countByStatus(ONGOING);
                long totalCompletedOrders = orderRepository.countByStatus(COMPLETED);
                long totalCancelledOrders = orderRepository.countByStatus(CANCELLED);

                OrderStatisticResponse response = OrderStatisticResponse.builder()
                                .totalOrders(totalOrders)
                                .byStatus(new OrderByStatus(
                                                totalOngoingOrders, totalCompletedOrders, totalCancelledOrders))
                                .build();

                log.info(ORDER_STATISTIC);

                return new ResponseEntity<>(response, statusOk);
        }
}
