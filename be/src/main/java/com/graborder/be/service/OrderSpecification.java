package com.graborder.be.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.graborder.be.dto.request.OrderFilterRequest;
import com.graborder.be.models.olap.Order;

import jakarta.persistence.criteria.Predicate;

public class OrderSpecification {
    public static Specification<Order> filterOrder(OrderFilterRequest request){
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if(request.getCustomerName() != null){
                String searchValue = "%" + request.getCustomerName() + "%";
                Predicate customerPredicate = criteriaBuilder.like(root.get("user").get("fullname"), searchValue);
                predicates.add(customerPredicate);
            }

            if (request.getFoodName() != null) {
                String searchValue = "%" + request.getFoodName() + "%";
                Predicate foodPredicate = criteriaBuilder.like(root.get("food").get("name"), searchValue);
                predicates.add(foodPredicate);
            }

            if (request.getStatus() != null) {
                Predicate statusPredicate = criteriaBuilder.equal(root.get("status"), request.getStatus());
                predicates.add(statusPredicate);
            }

            if (request.getOrderDate() != null) {
                Predicate requestDatePredicate = criteriaBuilder.equal(
                        criteriaBuilder.function("date", LocalDate.class, root.get("orderDate")),
                        request.getOrderDate());
                predicates.add(requestDatePredicate);
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}