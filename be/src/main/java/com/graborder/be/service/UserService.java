package com.graborder.be.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.graborder.be.dto.request.LoginRequest;
import com.graborder.be.dto.request.RegisterRequest;
import com.graborder.be.dto.response.MessageResponse;
import com.graborder.be.dto.response.ResponseBodyDTO;
import com.graborder.be.dto.response.UserInfoResponse;
import com.graborder.be.exception.classes.BadRequestException;
import com.graborder.be.models.oltp.UserMongo;
import com.graborder.be.repositories.UserOltpRepository;
import com.graborder.be.service.kafkaservice.KafkaProducerService;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService{
    
    @Autowired
    private UserOltpRepository userRepository;

    @Autowired
    private KafkaProducerService kafkaProducerService;

    @Autowired
    private GenerateSequenceService sequenceService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MongoTemplate mongoTemplate;

    private static final HttpStatus statusOk = HttpStatus.OK;

    @Transactional
    public ResponseEntity<Object> registerUser(RegisterRequest request){
        UserMongo userExist = userRepository.findByUsername(request.getUsername());

        if(userRepository.existsByUsername(request.getUsername())){
            String message = messageSource.getMessage("username.exists", null, Locale.getDefault());
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse(message, HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase()));
        }else if(!request.getPassword().equals(request.getRetypePassword())){
            String message = messageSource.getMessage("retype.password.mismatch", null, Locale.getDefault());
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse(message, HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase()));
        } else {
            UserMongo newUser = UserMongo.builder()
                .id(sequenceService.generateSequence(UserMongo.SEQUENCE_NAME))
                .username(request.getUsername())
                .fullname(request.getFullname())
                .password(passwordEncoder.encode(request.getPassword()))
                .isDeleted(false)
                .build();
            
            userRepository.save(newUser);
            kafkaProducerService.sendUserData(newUser);

            String message = "Berhasil register user: " + request.getUsername() + " !";
            log.info(message);

            MessageResponse response = MessageResponse.builder()
                .message(message)
                .statusCode(statusOk.value())
                .status(statusOk.getReasonPhrase())
                .build();
            
            return new ResponseEntity<>(response, statusOk);
        }
    }

    public UserMongo findByUsernameAndPassword(String username, String password) {
        Query query = new Query(Criteria.where("username").is(username));
        UserMongo user = mongoTemplate.findOne(query, UserMongo.class);

        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
            return user;
        }

        return null;
    }

    public ResponseEntity<Object> loginUser(LoginRequest request) {
        UserMongo userExist = findByUsernameAndPassword(request.getUsername(), request.getPassword());

        if (userExist == null) {
            throw new BadRequestException("Username atau password salah");
        } else {
            String message = "Berhasil Login dengan username " + request.getUsername();

            ResponseBodyDTO response = ResponseBodyDTO.builder()
                    .total(1)
                    .data(new UserInfoResponse(userExist.getId(), userExist.getUsername()))
                    .message(message)
                    .code(statusOk.value())
                    .status(statusOk.getReasonPhrase())
                    .build();

            log.info(message);

            return new ResponseEntity<>(response, statusOk);
        }
    }
}
