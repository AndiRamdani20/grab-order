package com.graborder.be.service.kafkaservice;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graborder.be.models.olap.Order;
import com.graborder.be.models.olap.User;
import com.graborder.be.models.oltp.CartMongo.FoodCartInfo;
import com.graborder.be.models.oltp.OrderMongo;
import com.graborder.be.models.oltp.UserMongo;
import com.graborder.be.repositories.FoodRepository;
import com.graborder.be.repositories.OrderRepository;
import com.graborder.be.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaConsumerService {
    
    @Autowired
    UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private FoodRepository foodRepository;

    @KafkaListener(topics = "${spring.kafka.topic.user}", groupId = "user")
    public void consumeDataUser(String users) {
        log.info("userData is: " + users);
        
        ObjectMapper mapper = new ObjectMapper();
        try{
            UserMongo userMongo = mapper.readValue(users, UserMongo.class);
            User newUser = User.builder()
                .userId(userMongo.getId())
                .username(userMongo.getUsername())
                .fullname(userMongo.getFullname())
                .password(userMongo.getPassword())
                .isDeleted(userMongo.getIsDeleted())
                .createdTime(Timestamp.valueOf(LocalDateTime.now()))
                .build();

            userRepository.save(newUser);

            log.info("Berhasil menyimpan data ke OLAP Database");
        } catch (JsonProcessingException e){
            log.error("Error getting user data: ", e);
        }
    }

    @KafkaListener(topics = "${spring.kafka.topic.order}", groupId = "orders")
    public void consumeDataOrder(String orderData){
        log.info("Received order data: {}", orderData);

        ObjectMapper mapper = new ObjectMapper();
        try{
            OrderMongo orderMongo = mapper.readValue(orderData, OrderMongo.class);
            Optional<Order> orderExistData = orderRepository.findById(orderMongo.getId());

            if(orderExistData.isEmpty()){
                Optional<User> user = userRepository.findById(orderMongo.getCustomer().getId());
                // Optional<Food> food = foodRepository.findById(orderMongo.getDetail().getFood().getId());
                
                List<String> listFood = orderMongo.getDetail().getFoodCart().stream().map(FoodCartInfo::getFoodName).toList();

                Order newOrder = Order.builder()
                    .orderId(orderMongo.getId())
                    .user(user.orElseThrow(() -> new RuntimeException("User not found with ID: " + orderMongo.getCustomer().getId())))
                    // .food(food.orElseThrow(() -> new RuntimeException("Food not found with ID: " + orderMongo.getDetail().getFood().getId())))
                    .orderDate(Timestamp.valueOf(orderMongo.getOrderDate()))
                    .qty(orderMongo.getQty())
                    .totalPrice(orderMongo.getTotal())
                    .status(orderMongo.getStatus())
                    .createdTime(Timestamp.valueOf(orderMongo.getOrderDate()))
                    .foodName(listFood)
                    .build();
                
                orderRepository.save(newOrder);

                log.info("Successfully saved order data to OLAP Database");
            } else {
                Order existingOrder = orderExistData.get();
                log.info("Order data already exists in OLAP Database: {}", existingOrder);
                existingOrder.setStatus(orderMongo.getStatus());
                existingOrder.setModifiedTime(Timestamp.valueOf(LocalDateTime.now()));
                orderRepository.save(existingOrder);

                log.info("Successfully updated status ({}) in OLAP Database", orderMongo.getStatus());
            }
        } catch(JsonProcessingException e){
            log.error("Error processing order data: ", e);
            // Handle the error appropriately
        }
    }
}
