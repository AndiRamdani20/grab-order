package com.graborder.be.service.kafkaservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graborder.be.models.oltp.OrderMongo;
import com.graborder.be.models.oltp.UserMongo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaProducerService {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${spring.kafka.topic.user}")
    String kafkaTopicUser;

    @Value("${spring.kafka.topic.order}")
    String kafkaTopicOrder;

    public void sendUserData(UserMongo users){
        ObjectMapper mapper = new ObjectMapper();
        try{
            String json = mapper.writeValueAsString(users);
            log.info("Sending user data");
            kafkaTemplate.send(kafkaTopicUser, json);
        } catch (JsonProcessingException e){
            log.error("Error serializing data object to json:", e);
        }
    }

    public void sendOrderData(OrderMongo order){
        ObjectMapper mapper = new ObjectMapper();
        try{
            String json = mapper.writeValueAsString(order);
            log.info("Sending order data: {}", json);
            kafkaTemplate.send(kafkaTopicOrder, json);
        } catch (JsonProcessingException e){
            log.error("Error serializing order data to JSON:", e);
            // Handle the error appropriately
        }
    }
    
}
