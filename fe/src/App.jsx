import { BrowserRouter, Route, Routes } from 'react-router-dom'; // Perbaikan: Menggunakan BrowserRouter dan Route
import Cart from './pages/Cart';
import Home from './pages/Home';
import Login from './pages/Login';
import Order from './pages/Order';
import OrderDetail from './pages/OrderDetail';
import OrderList from './pages/OrderList';
import Register from './pages/Register';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/home" element={<Home />} />
        <Route path="/keranjang" element={<Cart />} />
        <Route path="/order-progress" element={<Order />} />
        <Route path="/order-list" element={<OrderList />} />
        <Route path="/order/:id" element={<OrderDetail />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
