import axios from 'axios';
import { useEffect, useState } from 'react';
import ayamGeprekImg from "../assets/ayam-geprek.jpeg";
import Navbar from '../components/Navbar';

const Cart = () => {
    const [open, setOpen] = useState(true);
    const [cartData, setCartData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function fetchCartData() {
            try {
                const userId = localStorage.getItem("userId");
                const response = await axios.get(`http://localhost:8080/food-order/cart/${userId}`);
                setCartData(response.data.data);
                setIsLoading(false);
            } catch (error) {
                console.error("Error fetching cart data:", error);
                setIsLoading(false);
            }
        }
        fetchCartData();
    }, []);

    const clickCreateOrder = async () => {
        const userId = localStorage.getItem("userId");
    
        await axios
            .post(`http://localhost:8080/food-order/order/${userId}`)
            .then((response) => {
                console.log({ response });
            })
            .catch((error) => {
                console.log({ error });
            });
    };

    const addToCart = async (foodId) => {
        try {
            await axios.post("http://localhost:8080/food-order/add-to-cart", { 
                userId: localStorage.getItem("userId"), 
                foodId: foodId 
            });
            // Setelah berhasil menambahkan, perbarui cartData
            const updatedCartData = { ...cartData };
            const productIndex = updatedCartData.foods.findIndex(item => item.foodId === foodId);
            if (productIndex !== -1) {
                updatedCartData.foods[productIndex].qty++;
                setCartData(updatedCartData);
            }
        } catch (error) {
            alert("Failed to add item to cart.");
            console.error("Error adding item to cart:", error);
        }
    };
    
    const deleteFromCart = async (foodId) => {
        try {
            await axios.delete("http://localhost:8080/food-order/delete-from-cart", { 
                data: { 
                    userId: localStorage.getItem("userId"), 
                    foodId: foodId 
                } 
            });
            // Setelah berhasil menghapus, perbarui cartData
            const updatedCartData = { ...cartData };
            const productIndex = updatedCartData.foods.findIndex(item => item.foodId === foodId);
            if (productIndex !== -1) {
                updatedCartData.foods[productIndex].qty--;
                if (updatedCartData.foods[productIndex].qty < 0) {
                    updatedCartData.foods[productIndex].qty = 0;
                }
                setCartData(updatedCartData);
            }
        } catch (error) {
            alert("Failed to remove item from cart.");
            console.error("Error removing item from cart:", error);
        }
    };
    
    

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (!cartData) {
        return <div>Error fetching cart data</div>;
    }

    return (
        <>
            <Navbar />
            <div className="flex items-center bg-red-500 justify-center min-w-screen">
                <div className="bg-white overflow-hidden shadow-xl sm:max-w-2xxl w-full">
                    <div className="px-4 py-6 sm:px-6">
                    <h2 className="text-lg font-semibold text-gray-900">Food Cart</h2>
                    </div>

                    <div className="px-4 py-6 sm:px-6">
                    <ul role="list" className="-my-6 divide-y divide-gray-200">
                        {cartData.foods.map((product) => (
                        <li key={product.foodId} className="flex py-6">
                            <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                            <img
                                src={ayamGeprekImg}
                                className="h-full w-full object-cover object-center"
                            />
                            </div>

                            <div className="ml-4 flex flex-1 flex-col">
                            <div>
                                <div className="flex justify-between text-base font-medium text-gray-900">
                                <h3>
                                    <a href="">{product.foodName}</a>
                                </h3>
                                <p className="ml-4">Rp. {product.price}</p>
                                </div>
                                <p className="mt-1 text-sm text-gray-500"></p>
                            </div>
                            <div className="flex flex-1 items-end justify-between text-sm">
                                <p className="text-gray-500">Quantity <span className='font-bold'>{product.qty}</span></p>

                                <div className="flex">
                                    <button onClick={() => deleteFromCart(product.foodId)} className="border-2 border-gray-500 text-gray-500 px-2 py-2 rounded-full hover:bg-gray-700 hover:text-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                        </svg>
                                    </button>
                                    <h3 className="mx-2 mt-1 text-xl text-center align-middle">
                                        <span className="text-gray-500">
                                            {product.qty}
                                        </span>
                                    </h3>
                                    <button onClick={() => addToCart(product.foodId)} className="bg-gray-600 text-white px-2 py-2 rounded-full hover:bg-indigo-700">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            </div>
                        </li>
                        ))}
                    </ul>
                    </div>

                    <div className="border-t border-gray-200 px-4 py-6 sm:px-6">
                        <div className="flex justify-between text-base font-medium text-gray-900">
                            <p>Subtotal</p>
                            <p>Rp. {cartData.totalPrice}</p>
                        </div>
                        <p className="mt-0.5 text-sm text-gray-500">Shipping and taxes calculated at checkout.</p>
                        <div className="mt-6">
                            <a type='button' onClick={clickCreateOrder} href="/order-progress" className="flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-indigo-700">Checkout</a>
                        </div>
                        <div className="mt-6 flex justify-center text-center text-sm text-gray-500">
                            <p>or{' '}
                                <a href="/home">
                                    <button type="button" className="font-medium text-indigo-600 hover:text-indigo-500">Continue Shopping <span aria-hidden="true">&rarr;</span></button>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Cart;