import axios from "axios";
import { useEffect, useState } from "react";
import ayamGeprekImg from "../assets/ayam-geprek.jpeg";
import Navbar from "../components/Navbar";

const Home = () => {

    const [foodData, setFoodData] = useState([]);
    const [cartData, setCartData] = useState({});
    const [error, setError] = useState([]);

    useEffect(() => {
        async function fetchFoodData() {
            await axios
                .get("http://localhost:8080/food-order/foods")
                .then((response) => {
                        setFoodData(response.data.data);
                })
                .catch((error) => {
                    console.log(error);
                    setError(true);
            });
        }
        fetchFoodData();
    }, []);

    useEffect(() => {
        async function fetchCartData() {
            try {
                const userId = localStorage.getItem("userId");
                const response = await axios.get(`http://localhost:8080/food-order/cart/${userId}`);
                setCartData(response.data.data);
            } catch (error) {
                setError(error);
            }
        }
        fetchCartData();
    }, []);

    const addToCart = async (foodId) => {
        console.log("foodId:", foodId);
        try {
            await axios.post("http://localhost:8080/food-order/add-to-cart", { 
                userId: localStorage.getItem("userId"), 
                foodId: foodId });
        } catch (error) {
            alert("Failed to add item to cart.");
            console.error("Error adding item to cart:", error);
        }
    };

    const deleteFromCart = async (foodId) => {
        try {
            await axios.delete("http://localhost:8080/food-order/delete-from-cart", { 
                data: { 
                    userId: localStorage.getItem("userId"), 
                    foodId: foodId } });
        } catch (error) {
            alert("Failed to remove item from cart.");
            console.error("Error removing item from cart:", error);
        }
    };

    return (
        <>
            <Navbar />
            <div className="bg-white">
                <div className="text-center mt-7">
                    <h6 className="mt-4 text-3xl tracking-tight text-gray-900 sm:text-5xl">Daftar Makanan</h6>
                </div>
                <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
                    <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
                    {foodData.map((data) => (
                        <a key={data.id} className="group shadow-lg p-3 rounded-xl">
                            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg bg-gray-500 xl:aspect-h-8 xl:aspect-w-7">
                                <img
                                src={ayamGeprekImg}
                                className="h-full w-full object-cover object-center group-hover:opacity-75"
                                />
                            </div>
                            <h3 className="mt-4 text-sm text-gray-700">{data.foodName}</h3>
                            <p className="mt-1 text-lg font-medium text-gray-900">{data.price}</p>
                            <div className="flex mt-2">
                                <button onClick={() => deleteFromCart(data.id)} className="border-2 border-gray-500 text-gray-500 px-2 py-2 rounded-full hover:bg-gray-700 hover:text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                    </svg>
                                </button>
                                <h3 className="mx-2 mt-1 text-xl text-center align-middle"><span className="text-gray-500">{cartData.foods && cartData.foods.find(item => item.foodId === data.id)?.qty || 0}</span></h3>
                                <button onClick={() => addToCart(data.id)} className="bg-gray-600 text-white px-2 py-2 rounded-full hover:bg-indigo-700">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                    </svg>
                                </button>
                            </div>
                        </a>
                    ))}
                    </div>
                </div>
            </div>
        </>
    )
};

export default Home;