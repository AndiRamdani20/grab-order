import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import ayamGeprek from "../assets/ayam-geprek.jpeg";
import Navbar from '../components/Navbar';

const OrderDetail = ({ match }) => {
    const { id } = useParams();
    const [orderData, setOrderData] = useState();
    const [formattedDate, setFormattedDate] = useState();
    const [isError, setIsError] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [successMessage, setSuccessMessage] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        async function fetchOrderData() {
            await axios
            .get(`http://localhost:8080/food-order/order/${id}`)
            .then((response) => {
                console.log({ response });
                setOrderData(response.data.data);
    
                const timestamp = response.data.data.orderDate;
                const options = {
                    year: "numeric",
                    month: "numeric",
                    day: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                    second: "numeric",
                };
    
                setFormattedDate(
                    new Intl.DateTimeFormat("id-ID", options).format(
                    new Date(timestamp)
                    )
                );
            })
            .catch((error) => {
                console.log({ error });
                setIsError(true);
            });
        }
    
        fetchOrderData();
    }, [id]);

    const clickActionOrder = async (action) => {
    await axios
        .put("http://localhost:8080/food-order/order/update", {
        orderId: orderData.id,
        action: action,
        })
        .then((response) => {
        console.log({ response });
        setIsSuccess(true);

        if (action === "Cancelled") {
            setSuccessMessage("Pesanan anda berhasil Dicancel.");
        } else if (action === "Completed") {
            setSuccessMessage("Pesanan anda berhasil Diterima.");
        }

        setTimeout(() => {
            navigate("/home");
        }, 3000);
        })
        .catch((error) => {
        console.log({ error });
        setIsError(true);
        });
    };

    return (
        <>
        <Navbar/>
            {orderData && (
                <div className="flex items-center justify-center min-h-screen">
                    <div className="bg-white rounded-lg overflow-hidden shadow-xl sm:max-w-2xl w-full">
                        {/* Header */}
                        <div className="px-4 py-6 sm:px-6">
                            <h2 className="text-lg font-semibold text-gray-900">Food Cart</h2>
                        </div>
    
                        {/* List pesanan */}
                        <div className="px-4 py-6 sm:px-6">
                            <ul role="list" className="-my-6 divide-y divide-gray-200">
                            {orderData.detail.foodCart.map((data) => (
                                <li key={data.foodId} className="flex py-6">
                                    <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                                        <img
                                            src={ayamGeprek}
                                            className="h-full w-full object-cover object-center"
                                        />
                                    </div>
                                    <div className="ml-4 flex flex-1 flex-col">
                                        <div>
                                            <div className="flex justify-between text-base font-medium text-gray-900">
                                                <h3>
                                                    <a href="">{data.foodName}</a>
                                                </h3>
                                                <p className="ml-4">Rp. {data.price}</p>
                                            </div>
                                            <p className="mt-1 text-sm text-gray-500"></p>
                                        </div>
                                        <div className="flex flex-1 items-end justify-between text-sm">
                                            <p className="text-gray-500">Quantity <span className='font-bold'>{data.qty}</span></p>
                                        </div>
                                    </div>
                                </li>
                            ))}
                            </ul>
                        </div>
                        <div className="px-4 py-6 sm:px-6">
                            <div className="flex justify-between text-base font-medium text-gray-900">
                                <h3 className="text-lg font-semibold text-gray-900">Total order makanan</h3>
                                <h3 className="text-lg font-semibold text-gray-900">{orderData.qty}</h3>
                            </div>
                            <div className="flex justify-between text-base font-medium text-gray-900">
                                <h3 className="text-lg font-semibold text-gray-900">Total harga</h3>
                                <h3 className="text-lg font-semibold text-gray-900">{orderData.total}</h3>
                            </div>
                            <div className="flex justify-between text-base font-medium text-gray-900">
                                <h3 className="text-lg font-semibold text-gray-900">Status Pesanan</h3>
                                <h3 className="text-lg font-semibold text-gray-900">{orderData.status}</h3>
                            </div>
                            <div className="flex justify-center text-base font-medium text-gray-900 mt-5">
                                <button 
                                    className="mr-5 flex items-center justify-center rounded-md border border-transparent bg-red-600 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-red-700"
                                    onClick={() => {clickActionOrder("Cancelled");}}>
                                        Batalkan Pesanan
                                </button>
                                <button
                                    className="flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-indigo-700"
                                    onClick={() => {clickActionOrder("Completed");}}>
                                        Pesanan Diterima
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );    
};

export default OrderDetail;
