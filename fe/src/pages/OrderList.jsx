import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Navbar from '../components/Navbar';

const OrderList = () => {
    const userId = localStorage.getItem("userId");
    const [orderData, setOrderData] = useState([]);
    const [isError, setIsError] = useState(false);
    const [isEmpty, setIsEmpty] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        async function fetchOrderData() {
            await axios
                .get(`http://localhost:8080/food-order/order/users?userId=${userId}`)
                .then((response) => {
                    setOrderData(response.data.data);
                })
                .catch((error) => {
                    console.log({ error });
                    if (error.response.status === 404) {
                    setIsError(false);
                    setIsEmpty(true);
                    } else {
                    setIsError(true);
                    }
                });
            }
            fetchOrderData();
        }, [userId]);

    return (
        <>
            <Navbar />
            <div className="flex items-center justify-center min-h-screen">
                <div className="bg-white rounded-lg overflow-hidden shadow-xl sm:max-w-2xl w-full">
                    <div className="px-4 py-6 sm:px-6">
                        <h2 className="text-lg font-semibold text-gray-900">Food Cart</h2>
                    </div>

                    <div className="px-4 py-6 sm:px-6">
                        <ul role="list" className="-my-6 divide-y divide-gray-200">
                            {orderData.map((data) => (
                                <li key={data.orderId} className="flex py-6" onClick={() => navigate(`/order/${data.orderId}`)}>
                                    <div className="ml-4 flex flex-1 flex-col">
                                        <div>
                                            <div className="flex justify-between text-base font-medium text-gray-900">
                                                <h3><a href="">{data.status}</a></h3>
                                                <p className="ml-4">{data.totalPrice}</p>
                                            </div>
                                            <p className="mt-1 text-sm text-gray-500"></p>
                                        </div>
                                        <div className="flex flex-1 items-end justify-between text-sm">
                                            <p className="text-gray-500">Qty {data.qty}</p>
                                            <div className="flex">
                                                <button type="button" className="font-medium text-indigo-600 hover:text-indigo-500">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}

export default OrderList;